SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account_role
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog`(
  id int PRIMARY KEY AUTO_INCREMENT,
  create_date DATETIME NOT NULL COMMENT '创建时间',
  user_id INT NOT NULL COMMENT '创建人ｉｄ',
  title VARCHAR(200) NOT NULL COMMENT '标题',
  content LONGTEXT NOT NULL COMMENT '正文',
  category_id int NOT NULL COMMENT '类别ｉｄ'
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;