package com.whereta.mapper;

import com.whereta.model.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Component("accountMapper")
public interface AccountMapper {
    /**
     * 根据主键删除
     * 参数:主键
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入，空属性也会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insert(Account record);

    /**
     * 插入，空属性不会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insertSelective(Account record);

    /**
     * 根据主键查询
     * 参数:查询条件,主键值
     * 返回:对象
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    Account selectByPrimaryKey(Integer id);

    /**
     * 根据主键修改，空值条件不会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKeySelective(Account record);

    /**
     * 根据主键修改，空值条件会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKey(Account record);

    Account getByAccount(@Param("account") String account);

    List<Map> selectAccountManage(@Param("depIdSet") Set<Integer> depIdSet, @Param("excludeAccountIdSet") Set<Integer> excludeAccountIdSet);
}