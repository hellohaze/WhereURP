package com.whereta.dao;

import com.whereta.model.AccountRole;

import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 17:05
 */
public interface IAccountRoleDao {

    Set<Integer> selectRoleIdSet(int accountId);

    int createAccountRole(AccountRole accountRole);

    int deleteByRoleId(int roleId);

    int deleteByAccountId(int accountId);

}
