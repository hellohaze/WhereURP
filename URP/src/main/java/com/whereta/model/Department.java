package com.whereta.model;

import java.io.Serializable;

public class Department implements Serializable {
    /**
     * department.id
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer id;

    /**
     * department.parent_id (上级部门id)
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer parentId;

    /**
     * department.name (部门名字)
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private String name;

    /**
     * department.order (排序)
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}