package com.whereta.model;

import java.io.Serializable;

public class DepartmentAccount implements Serializable {
    /**
     * department_account.id
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer id;

    /**
     * department_account.dep_id (部门id)
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer depId;

    /**
     * department_account.account_id (账号id)
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    private Integer accountId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}