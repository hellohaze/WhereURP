package com.whereta.controller.main;

import com.whereta.service.IRoleService;
import com.whereta.vo.ResultVO;
import com.whereta.vo.RoleCreateVO;
import com.whereta.vo.RoleEditVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Vincent
 * @time 2015/9/1 16:28
 */
@Controller
@RequestMapping("/main/role")
public class RoleController {
    @Resource
    private IRoleService roleService;

    //跳转到角色管理页面
    @RequestMapping("/manage")
    public String manage() {
        return "main/role/manage";
    }

    //获取显示的角色
    @RequestMapping(value = "/show-roles", method = RequestMethod.POST)
    public
    @ResponseBody
    ResultVO getShowRoles(@RequestParam(required = false) Integer checkUserId) {
        Subject subject = SecurityUtils.getSubject();
        Object principal = subject.getPrincipal();
        ResultVO resultVO = roleService.getShowRoles(Integer.parseInt(principal.toString()),checkUserId);
        return resultVO;
    }

    //创建角色
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public
    @ResponseBody
    ResultVO createRole(@Valid @ModelAttribute RoleCreateVO createVO, BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        ResultVO resultVO = new ResultVO(true);

        if (fieldErrors != null && !fieldErrors.isEmpty()) {
            String defaultMessage = fieldErrors.get(0).getDefaultMessage();
            resultVO.setOk(false);
            resultVO.setMsg(defaultMessage);
            return resultVO;
        }
        Subject subject = SecurityUtils.getSubject();
        Object principal = subject.getPrincipal();
        resultVO = roleService.createRole(createVO, Integer.parseInt(principal.toString()));
        return resultVO;
    }

    //编辑角色
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public
    @ResponseBody
    ResultVO editRole(@Valid @ModelAttribute RoleEditVO editVO, BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        ResultVO resultVO = new ResultVO(true);

        if (fieldErrors != null && !fieldErrors.isEmpty()) {
            String defaultMessage = fieldErrors.get(0).getDefaultMessage();
            resultVO.setOk(false);
            resultVO.setMsg(defaultMessage);
            return resultVO;
        }
        resultVO = roleService.editRole(editVO);
        return resultVO;
    }

    //删除角色
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    ResultVO delRole(@RequestParam int id) {
        ResultVO resultVO = roleService.deleteRole(id);
        return resultVO;
    }

    //角色授权
    @RequestMapping(value = "/grant", method = RequestMethod.POST)
    public
    @ResponseBody
    ResultVO grantRolePermission(@RequestParam int id,@RequestParam(value = "peridArray[]",required = false) Integer []peridArray) {
        ResultVO resultVO = roleService.grantPermissions(id,peridArray);
        return resultVO;
    }

}
