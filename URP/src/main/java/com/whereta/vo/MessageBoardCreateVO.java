package com.whereta.vo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by vincent on 15-9-25.
 */
public class MessageBoardCreateVO {
    @NotNull(message = "请输入留言内容")
    @Length(max = 1000,message = "阁下,1000个字符还无法表达您的愤怒吗?",min = 1)
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
